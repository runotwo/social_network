"""social_network URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path

from .views import (
    ConfirmAuthToken,
    CreateAuthToken,
    UserProfileList,
    MeUserProfile,
    Logout
)

urlpatterns = [
    path('users/', UserProfileList.as_view({'get': 'list'})),
    path('users/me/', MeUserProfile.as_view()),
    path('users/<int:pk>/', UserProfileList.as_view({'get': 'retrieve'})),
    path('auth/login/', CreateAuthToken.as_view()),
    path('auth/logout/', Logout.as_view()),
    path('auth/confirmation/', ConfirmAuthToken.as_view()),
]
