import datetime

from django.core.validators import RegexValidator
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from .models import UserProfile


class UserProfileSerializer(serializers.ModelSerializer):
    birthday_date = serializers.DateField(format='%d.%m.%Y')

    class Meta:
        model = UserProfile
        fields = ('id', 'phone_number', 'name', 'about', 'birthday_date')


class MyProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserProfile
        fields = '__all__'


class CreateTokenSerializer(serializers.Serializer):
    phone_number = serializers.CharField(required=True, validators=[RegexValidator(r'^\+7\d{10}$')])


class ConfirmTokenSerializer(serializers.Serializer):
    code = serializers.CharField(required=True, validators=[RegexValidator(r'^\d*$')])


class MeUserProfileSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    phone_number = serializers.CharField(read_only=True)
    name = serializers.CharField(max_length=80, required=False)
    about = serializers.CharField(required=False)
    birthday_date = serializers.DateField(format='%d.%m.%Y', input_formats=['%d.%m.%Y'], required=False)
    birthday_date_changes_count = serializers.IntegerField(read_only=True)

    class Meta:
        model = UserProfile
        fields = '__all__'

    def validate_birthday_date(self, birtday_date):
        if birtday_date >= datetime.date.today():
            raise ValidationError('Must be less than today')
        if self.instance.birthday_date_changes_count >= 3:
            raise ValidationError('Cannot be updated more than 3 times.')
        return birtday_date

    def update(self, instance, validated_data):
        new_birthday_date = validated_data.get('birthday_date')
        if new_birthday_date and new_birthday_date != instance.birthday_date:
            instance.birthday_date_changes_count += 1
        super().update(instance, validated_data)
