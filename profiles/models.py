from django.core.validators import RegexValidator
from django.db import models


class UserProfile(models.Model):
    phone_number = models.CharField(max_length=20, unique=True,
                                    validators=[RegexValidator(r'^\+7\d{10}$')], db_index=True)
    name = models.CharField(max_length=80, default='', blank=True)
    about = models.TextField(default='', blank=True)
    birthday_date = models.DateField(null=True, blank=True)
    birthday_date_changes_count = models.PositiveIntegerField(default=0)

    def save(self, *args, **kwargs):
        self.full_clean()
        super().save(*args, **kwargs)
