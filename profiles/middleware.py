from django.core.cache import cache
from django.utils.deprecation import MiddlewareMixin

from social_network.settings import API_TOKEN
from .models import UserProfile


class RedisAuthMiddleware(MiddlewareMixin):

    def process_request(self, request):
        request.is_api_server = False
        request.is_temporary = False
        request.is_permanent = False
        request.user_profile = None
        auth_header = request.headers.get('Authorization')
        if not auth_header:
            return
        try:
            auth_type, token = auth_header.split()
        except ValueError:
            return
        if token == API_TOKEN:
            request.is_api_server = True
            return
        cache_value = cache.get(token)
        if not cache_value:
            return
        if cache_value.endswith('__perm'):
            request.is_permanent = True
            phone_number = cache_value.rstrip('__perm')
            request.user_profile, _ = UserProfile.objects.get_or_create(phone_number=phone_number)
            return
        if cache_value.endswith('__temp'):
            request.is_temporary = True

    def process_response(self, request, response):
        return response
