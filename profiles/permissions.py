from rest_framework import permissions


class IsTemporaryRequest(permissions.BasePermission):

    def has_permission(self, request, view):
        return request.is_temporary


class IsPermanentRequest(permissions.BasePermission):

    def has_permission(self, request, view):
        return request.is_permanent


class IsApiServerRequest(permissions.BasePermission):

    def has_permission(self, request, view):
        return request.is_api_server
