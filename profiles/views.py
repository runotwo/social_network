import random
from string import digits
from uuid import uuid4

from django.core.cache import cache
from rest_framework import views
from rest_framework import viewsets
from rest_framework.response import Response

from social_network.settings import TEMPORARY_TOKEN_TIMEOUT
from .models import UserProfile
from .permissions import (
    IsApiServerRequest,
    IsPermanentRequest,
    IsTemporaryRequest
)
from .serializers import (
    ConfirmTokenSerializer,
    CreateTokenSerializer,
    UserProfileSerializer,
    MeUserProfileSerializer
)


class UserProfileList(viewsets.ModelViewSet):
    queryset = UserProfile.objects.all()
    serializer_class = UserProfileSerializer
    permission_classes = [IsApiServerRequest | IsPermanentRequest]


class CreateAuthToken(views.APIView):
    serializer_class = CreateTokenSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        if not serializer.is_valid():
            return Response(serializer.errors, status=400)
        token = str(uuid4()).replace('-', '')
        phone_number = serializer.validated_data['phone_number']
        code = self.get_code()
        cache.set(f'{token}', f'{phone_number}__{code}__temp', timeout=TEMPORARY_TOKEN_TIMEOUT)
        return Response({'token': token, 'code': code}, status=201)

    @staticmethod
    def get_code(digits_count=6):
        return ''.join(random.choices(digits, k=digits_count))


class ConfirmAuthToken(views.APIView):
    serializer_class = ConfirmTokenSerializer
    permission_classes = [IsTemporaryRequest]

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        if not serializer.is_valid():
            return Response(serializer.errors, status=400)
        _, token = request.headers['Authorization'].split()
        cache_value = cache.get(token).rstrip('__temp')
        phone_number, code = cache_value.split('__')
        if code == serializer.validated_data['code']:
            cache.delete(token)
            perm_token = str(uuid4()).replace('-', '')
            cache.set(f'{perm_token}', f'{phone_number}__perm', timeout=None)
            return Response({'token': perm_token}, status=201)
        else:
            return Response({'code': ['Not matched']}, status=401)


class Logout(views.APIView):
    permission_classes = [IsPermanentRequest]

    def post(self, request, *args, **kwargs):
        _, token = request.headers['Authorization'].split()
        cache.delete(token)
        return Response()


class MeUserProfile(views.APIView):
    permission_classes = [IsPermanentRequest]
    serializer_class = MeUserProfileSerializer

    def get(self, request, *args, **kwargs):
        serializer = self.serializer_class(instance=request.user_profile)
        return Response(serializer.data, status=200)

    def patch(self, request, *args, **kwargs):
        serializer = self.serializer_class(instance=request.user_profile, data=request.data)
        if not serializer.is_valid():
            return Response(serializer.errors, status=400)
        serializer.update(serializer.instance, serializer.validated_data)
        return Response(serializer.data)
