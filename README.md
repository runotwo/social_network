# Deploy

Создаем .env файл туда вписываем все нужные credentials (можно скопировать `cp .env.sample .env`, там уже все есть)

```
POSTGRES_NAME - название бд
POSTGRES_HOST - хост бд
POSTGRES_USER - пользователь бд
POSTGRES_PASSWORD - пароль от бд

REDIS_HOST - url Redis'а

API_TOKEN - Токен для доступа с других серверов

TEMPORARY_TOKEN_TIMEOUT - время жизни временного токена
```

В корне проекта `docker-compose build && docker-compose up -d`  
```
docker-compose exec web python3 manage.py migrate
```
Апи готово к использованию по `0.0.0.0`

# Docs
В README доку написать немного быстрее, чем в swagger с:

#### Запрос смс
```
POST /api/auth/login/ => {"phone_number":"+79999999999"}
<= {"token":"","code":""}        получили {{temp_token}}
```
#### Подтверждение входа
```
POST /api/auth/login/ => {"code":"111111"}   Header Authorization: Token {{temp_token}}
<= {"token":""}           получили {{perm_token}}, далее подписываемся им
```
#### Logout
```
POST /api/auth/login/    Header Authorization: Token {{perm_token}}
<= status 200
```
#### Получение информации о себе
```
GET /api/users/me/       Header Authorization: Token {{perm_token}}
<= {"id":1,"phone_number":"+79999999999","name":"Denis","about":"about","birthday_date":"08.04.1999","birthday_date_changes_count":4}
```
#### Обновление информации о себе
```
PATCH /api/users/me/ => {"name":"","about":"","birthday_date":"dd.mm.yyyy"}      Header Authorization: Token {{perm_token}}
<= {"id":1,"phone_number":"+79999999999","name":"","about":"","birthday_date":"dd.mm.yyyy","birthday_date_changes_count":1}
```
#### Получение информации о других прользователях
```
GET /api/users/      Header Authorization: Token {{perm_token}}
<= [{"id":1,"phone_number":"+79999999999","name":"Denis","about":"about","birthday_date":"08.04.1999"}]
```
#### Получение информации о конкретном прользователе
```
GET /api/users/1/      Header Authorization: Token {{perm_token}}
<= {"id":1,"phone_number":"+79999999999","name":"Denis","about":"about","birthday_date":"08.04.1999"}
```
# Description

Nginx поднял только из-за того, что может появиться необходимость раздавать статику

Сделал немного странную авторизацию из-за того, что у hmset в Redis нельзя указать expiration, а на придумывание элегантного решения могло уйти много времени.

При авторизации используется временный токен, который действует n секунд или до подтверждения входа кодом. После подтверждения выдается новый постоянный токен, с которым есть доступ к апи.
Так же есть отдельный токен для интеграции внутренних сервисов, он прописывается в .env (можно было сделать лучше, но это ведь тестовое с:). 
С этим токеном есть доступ только к list и retrieve пользователей.

