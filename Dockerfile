FROM python:3.6.8

WORKDIR /opt/app

RUN pip install uwsgi

COPY requirements.txt .
RUN pip install -r requirements.txt

COPY ./ .

CMD ["uwsgi", "--ini", "/opt/app/uwsgi.ini"]

